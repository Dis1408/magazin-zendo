const mongoose = require('mongoose');

var CategorySchema = new mongoose.Schema({
	title: String,
	/*slug:  {
		type: String,
		unique: true
	},*/
	parent: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
	image: String
});

module.exports = mongoose.model('Category', CategorySchema);
