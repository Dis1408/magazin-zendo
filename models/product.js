const mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
	title: String,
	slug:  {
		type: String,
		unique: true
	},
	image: String,
	category: {
		type: mongoose.Schema.Types.ObjectId,
		ref:  'Category'
	}
});

module.exports = mongoose.model('Product', ProductSchema);
