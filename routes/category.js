const express = require('express'),
      router  = express.Router();

var Category = require('../models/category'),
    Product  = require('../models/product');

/* GET category listing. */
router.get('/', function(req, res, next) {
    Category.find({
        // only parent categories
        parent: null
    }, function(err, categories) {
        if (err) {
            return res.send(err);
        }
        res.json(categories);
    });
});

/* POST category listing. */
router.post('/', function(req, res, next) {
    //res.send('You are just a user, you can not create a category');

    // TODO comment
    // check if slug and title exists in POST data
    if (/*!req.body.slug
        || */!req.body.title) {
        return res.send('You should pass the `slug` and `title` params!');
    }

    // create a new instance of the Category model
    var category = new Category({
        title: req.body.title,
        slug:  req.body.slug,
        image: req.body.image,
        parent:  req.body.parent,
    });

    // save the category and check for errors
    category.save(function(err) {
        if (err) {
            return res.send(err);
        }
        res.send('Category has been created!');
    });
    
});

/* DELETE category listing. */
router.delete('/', function(req, res, next) {
    //res.send('You are just a user, you can not delete all categories');

    // TODO comment
    Category.remove({}, function(err, category) {
        if (err) {
            return res.send(err);
        }
        res.send('ALL categories have been deleted!');
    });
    
});

/* GET category id listing. Get child categories */
router.get('/:id', function(req, res, next) {
    //Category.findById(req.params.id, function(err, category) {
    Category.find({
        parent: req.params.id
    }, function(err, categories) {
        if (err) {
            return res.send(err);
        }

        // check if there are subcategories
        if (categories) {
            return res.json(categories);    
        }
        
        // if not - show the products
        // TODO
    });
});

/* PUT category id listing. */
router.put('/:id', function(req, res, next) {
    //res.send('You are just a user, you can not update a category');

    // TODO comment
    // check if slug and title exists in POST data
    if (/*!req.body.slug
        || */!req.body.title) {
        return res.send('You should pass the `slug` and `title` params!');
    }

    // create a new instance of the Category model
    //var category = Category.findById(req.params.id, function(err, category) {
    Category.findOne({
        _id: req.params.id
    }, function(err, category) {
        if (err) {
            return res.send(err);
        }

        // set the category title (comes from the request)
        category.title  = req.body.title;
        category.slug   = req.body.slug;
        category.image  = req.body.image;
        category.parent = req.body.parent;

        // save the category and check for errors
        category.save(function(err) {
            if (err) {
                return res.send(err);
            }
            res.send('Category has been updated!');
        });
    });
    
});

/* DELETE category id listing. */
router.delete('/:id', function(req, res, next) {
    //res.send('You are just a user, you can not delete this category');

    // TODO comment
    Category.remove({
        _id: req.params.id
    }, function(err, category) {
        if (err) {
            return res.send(err);
        }
        res.send('Category has been deleted!');
    });
    
});

/* GET category id products listing. */
router.get('/:id/products', function(req, res, next) {
    //Category.findById(req.params.id, function(err, category) {
    Category.findOne({
        _id: req.params.id
    }, function(err, category) {
        if (err) {
            return res.send(err);
        }
        
        // get products for this category
        Product.find({
            category: category
        }, function(err, product) {
            if (err) {
                return res.send(err);
            }
            res.json(product);
        });
    });
});

/* DELETE category id products listing. */
router.delete('/:id/products', function(req, res, next) {
    //res.send('You are just a user, you can not delete these products!');

    // TODO comment
    Category.findOne({
        _id: req.params.id
    }, function(err, category) {
        if (err) {
            return req.send(err);
        }

        // remove all products from this category
        Product.remove({
            category: category
        }, function(err) {
            if (err) {
                return req.send(err);
            }
            res.send('ALL products in this category have been deleted!');
        });
    });
});

module.exports = router;
