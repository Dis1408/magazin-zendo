const express = require('express'),
      router  = express.Router();

var Category = require('../models/category'),
    Product  = require('../models/product');

/* GET products listing. */
router.get('/', function(req, res, next) {
    Product.find({}, function(err, products) {
        if (err) {
            return res.send(err);
        }
        res.json(products);
    });
});

/* POST products listing. */
router.post('/', function(req, res, next) {
    //res.send('You are just a user, you can not create a products');

    // TODO comment
    // check if slug and title exists in POST data
    if (!req.body.slug
        || !req.body.title) {
        return res.send('You should pass the `slug` and `title` params!');
    }

    var product = new Product({
        title: req.body.title,
        slug:  req.body.slug,
        image: req.body.image
    });

    // get the category if exists
    if (req.body.category) {
        Category.findOne({
            slug: req.body.category
        }, function(err, category) {
            if (err) {
                return err;
            }
            
            // add category object ID to a product
            if (null !== category) {
                product.category = category._id;
            }

            // save the product and check for errors
            // it might be in a Schema file, as a function    
            product.save(function(err) {
                if (err) {
                    return res.send(err);
                }
                res.send('Product has been created!');
            });
        });
    }
});

/* DELETE products listing. */
router.delete('/', function(req, res, next) {
    //res.send('You are just a user, you can not delete all products');

    // TODO comment
    Product.remove({}, function(err) {
        if (err) {
            return res.send(err);
        }
        res.send('ALL products have been deleted!');        
    });
});


/*** /products/id ***/

/* GET product id listing. */
router.get('/:id', function(req, res, next) {
    Product.findOne({
        slug: req.params.id
    }, function(err, product) {
        if (err) {
            return res.send(err);
        }
        res.json(product);
    });
});

/* PUT product id listing. */
router.put('/:id', function(req, res, next) {
    //res.send('You are just a user, you can not update a product');

    // TODO comment
    // check if slug and title exists in POST data
    if (!req.body.slug
        || !req.body.title) {
        return res.send('You should pass the `slug` and `title` params!');
    }

    var product = Product.findOne({
        slug: req.params.id
    }, function(err, product) {
        if (err) {
            return res.send(err);
        }

        // set the category title and image (comes from the request)
        product.title = req.body.title;
        product.slug  = req.body.slug;
        product.image = req.body.image;

        // save the category and check for errors
        product.save(function(err) {
            if (err) {
                return res.send(err);
            }
            res.send('Product has been updated!');
        });
    });
});

/* DELETE product id listing. */
router.delete('/:id', function(req, res, next) {
    //res.send('You are just a user, you can not delete this product');

    // TODO comment
    Product.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            return res.send(err);
        }
        res.send('Product has been deleted!');        
    });
});

module.exports = router;
